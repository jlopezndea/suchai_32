/**
 * @file  os.h
 * @author Carlos Gonzalez Cortes
 * @author Ignacio Ibanez Aliaga
 * @date 2020
 * @copyright GNU Public License.
 *
 */

#ifndef _OS_H
#define _OS_H

#include "suchai/config.h"

#ifdef FREERTOS
    #include "freertos/FreeRTOSConfig.h"
    #include "freertos/FreeRTOS.h"
    #include "freertos/task.h"
 	#include "freertos/queue.h"
 	#include "freertos/semphr.h"
    #include "freertos/timers.h"
    #include "freertos/portable.h"
#else
    #define portMAX_DELAY (uint32_t) 0xffffffff
    #define pdPASS				     1
    #define configMINIMAL_STACK_SIZE 1
    #define portBASE_TYPE	short
    #define ClrWdt(){};
#endif


#endif //_OS_H
